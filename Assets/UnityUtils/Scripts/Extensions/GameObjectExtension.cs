﻿using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace UnityUtils.Extensions {

    public static class GameObjectExtension
    {

        /// <summary>
        /// Destroy the UnityObject.
        /// If in editor (not playing), DestroyImmediate
        /// If playing, just Destroy
        ///
        /// In build just Destroy is used
        /// </summary>
        /// <param name="obj"></param>
        public static void DestroyAnyway(Object obj)
        {
#if UNITY_EDITOR
            if (Application.isPlaying) GameObject.Destroy(obj);
            else GameObject.DestroyImmediate(obj);
#else
            GameObject.Destroy(obj);
#endif

        }

        /// <summary>
        /// Gets a component attached to the given game object.
        /// If one isn't found, a new one is attached and returned.
        /// </summary>
        /// <param name="gameObject">Game object.</param>
        /// <returns>Previously or newly attached component.</returns>
        public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
        {
            T component = gameObject.GetComponent<T>();
            return component != null ? component : gameObject.AddComponent<T>();
        }

        /// <summary>
        /// Get interface from this gameObject
        /// Uses simple access GetComponent(typeof(T))
        /// </summary>
        /// <typeparam name="T">The Interface</typeparam>
        /// <param name="gameObject">GameObject</param>
        /// <returns></returns>
        public static T GetInterface<T>(this GameObject gameObject) where T : class
        {
            return gameObject.GetComponent(typeof(T)) as T;
        }

        /// <summary>
        /// Get a list of interfaces from this gameObject
        /// Uses simple access GetComponents(typeof(T)), but must convert the array with a copy
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gameObject"></param>
        /// <returns></returns>
        public static T[] GetInterfaces<T>(this GameObject gameObject) where T : class
        {
            return ConvertToArray<T>(gameObject.GetComponents(typeof(T)));
        }

        /// <summary>
        /// Checks whether a game object has a component of type T attached.
        /// </summary>
        /// <param name="gameObject">Game object.</param>
        /// <returns>True when component is attached.</returns>
        public static bool HasComponent<T>(this GameObject gameObject) where T : Component
        {
            return gameObject.GetComponent<T>() != null;
        }

        /// <summary>
        /// Get a child by it's name
        /// You may perform a deepSearch, looking into all children 
        /// </summary>
        /// <param name="gameObject">Game object.</param>
        /// <param name="childName">The name to look for</param>
        /// <param name="deepSearch">Perform search in each children as well</param>
        /// <returns>the child or null</returns>
        public static GameObject GetChildNamed(this GameObject gameObject, string childName, bool deepSearch=false)
        {
            foreach (Transform transform in gameObject.transform)
            {
                if (transform.name == childName) return transform.gameObject;

                if (deepSearch)
                {
                    GameObject child = transform.gameObject.GetChildNamed(childName, true);
                    if (child != null) return child;
                }
            }

            return null;
        }

        /// <summary>
        /// Get children by regex pattern name
        /// </summary>
        /// <param name="gameObject">Game object.</param>
        /// <param name="regex">Regex name pattern</param>
        /// <param name="deepSearch">Perform search in each children as well</param>
        /// <returns>the child or null</returns>
        public static GameObject[] GetChildrenNamed(this GameObject gameObject, Regex regex, bool deepSearch = false)
        {
            List<GameObject> list = new List<GameObject>();
            gameObject.GetChildrenNamed(regex, deepSearch, list);
            return list.ToArray();
        }

        /// <summary>
        /// Get a child by it's name. Use regex to find a pattern
        /// </summary>
        /// <param name="gameObject">Game Object.</param>
        /// <param name="regex">Regex name pattern</param>
        /// <param name="deepSearch">Perform search in each children as well<</param>
        /// <returns>the child or null</returns>
        public static GameObject GetChildNamed(this GameObject gameObject, Regex regex, bool deepSearch = false)
        {
            foreach (Transform transform in gameObject.transform)
            {
                if (regex.IsMatch(transform.name)) return transform.gameObject;

                if (deepSearch)
                {
                    GameObject child = transform.gameObject.GetChildNamed(regex, true);
                    if (child != null) return child;
                }
            }

            return null;
        }

        /// <summary>
        /// Same as GetComponentInChildren from GameObject basic methods. However we do a deep search in each sub child
        /// </summary>
        /// <typeparam name="T">Component to seek</typeparam>
        /// <param name="gameObject">Game Object.</param>
        /// <param name="includeInactive">Include inactive game objects in search</param>
        /// <returns></returns>
        public static T GetComponentInChildrenRecursively<T>(this GameObject gameObject, bool includeInactive = false) where T: Component
        {
            T t = gameObject.GetComponentInChildren<T>(includeInactive);
            if (t != null) return t;

            foreach (Transform transform in gameObject.transform)
            {
                t = transform.gameObject.GetComponentInChildrenRecursively<T>(includeInactive);
                if (t != null) return t;
            }

            return null;
        }

        /// <summary>
        /// Same as GetComponentsInChildren from GameObject basic methods. However we do a deep search in each sub child
        /// </summary>
        /// <typeparam name="T">Component to seek</typeparam>
        /// <param name="gameObject">Game Object.</param>
        /// <param name="includeInactive">Include inactive game objects in search</param>
        /// <returns></returns>
        public static T[] GetComponentsInChildrenRecursively<T>(this GameObject gameObject, bool includeInactive = false) where T : Component
        {
            List<T> components = new List<T>();
            gameObject.GetComponentsInChildrenRecursively(includeInactive, components);
            return components.ToArray();
        }

        private static void GetComponentsInChildrenRecursively<T>(this GameObject gameObject, bool includeInactive, List<T> components) where T : Component
        {
            components.AddRange(gameObject.GetComponentsInChildren<T>(includeInactive));
            foreach (Transform transform in gameObject.transform)
            {
                transform.gameObject.GetComponentsInChildrenRecursively<T>(includeInactive, components);
            }
        }

        public static void GetChildrenNamed(this GameObject gameObject, Regex regex, bool deepSearch, List<GameObject> list)
        {
            foreach (Transform transform in gameObject.transform)
            {
                if (regex.IsMatch(transform.name)) list.Add(transform.gameObject);

                if (deepSearch)
                {
                    transform.gameObject.GetChildrenNamed(regex, true, list);
                }
            }
        }

        /// <summary>
        /// Apply to all children, recursively, this gameObject layer's 
        /// </summary>
        /// <param name="gameObject">Game Object</param>
        public static void ApplyLayerToChildren(this GameObject gameObject)
        {
            ApplyLayerToChildren(gameObject, gameObject.layer);
        }

        /// <summary>
        /// Apply to this gameObject and it's children, recursively, this specific layer
        /// </summary>
        /// <param name="gameObject">Game Object</param>
        /// <param name="layer">Layer</param>
        public static void ApplyLayerToChildren(this GameObject gameObject, int layer)
        {
            gameObject.layer = layer;
            foreach (Transform child in gameObject.transform)
            {
                ApplyLayerToChildren(child.gameObject, layer);
            }
        }

        public static bool IsAssetPrefab(this GameObject gameObject)
        {
            return !gameObject.scene.IsValid();
        }

        private static T[] ConvertToArray<T>(IList list)
        {
            T[] ret = new T[list.Count];
            list.CopyTo(ret, 0);
            return ret;
        }
    }

}
